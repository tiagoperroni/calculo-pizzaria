var formulario = document.querySelector("#formulario");

var pizzas = []

function cadastrar() {

    var pizza = []
    var nome = document.getElementById("nome").value;
    var tamanho = document.getElementById("tamanho").value;
    var preco = document.getElementById("preco").value;

    pizza.push(nome);
    pizza.push(tamanho);
    pizza.push(preco);

    var precoCm2 = calcularPrecoCm2(preco, tamanho);
    pizza.push(precoCm2);

    pizzas.push(pizza)    

    //calcular preco/cm2 
    function calcularPrecoCm2(preco, tamanho){
        var area = 3.14 * (tamanho/2) ** 2
        var precoCm2 = preco / area;
        return precoCm2;
    } 

    formulario.reset(); 
}

function relatorio() {

    pizzas.sort(function ordenar(a, b) {
        if (a[3] > b[3]){
            return 1;
        }
        if (b[3] > a[3]) {
            return -1;
        }
        return 0;     
});

    pizzas[0].push("Melhor CB");

    //calculando a diferença percentual das pizzas
    for (let i = 0; (i < pizzas.length - 1); i++) {
        let precoCm2A = pizzas[i][3];
        let precoCm2B = pizzas[i+1][3];
        
        let precoCm2 = ((precoCm2B / precoCm2A) - 1) * 100;
        pizzas[i + 1].push(precoCm2);
    }
        console.log(pizzas);


    //exibir o relatorio na tebela

    var tbodyRelatorio = document.getElementById("idTbody");
    for (let i = 0; i < pizzas.length; i++) {
        tbodyRelatorio.appendChild(montaTr(pizzas[i]));
        
    }

    function montaTr(pizza){            

        let dadosTr = document.createElement("tr");
        dadosTr.classList.add("tbody"); 

        dadosTr.appendChild(montaTd(pizza[0], "tdNome" ));
        dadosTr.appendChild(montaTd(pizza[1] + "cm", "tdTamanho" ));
        dadosTr.appendChild(montaTd("R$" + (pizza[2] * 1).toFixed(2), "tdPreco" ));
        dadosTr.appendChild(montaTd("R$" + pizza[3].toFixed(2), "tdPrecoMedio" ));       

        if(isNaN(pizza[4])){
            dadosTr.appendChild(montaTd(pizza[4], "tdMelhorCB" ));
        }else {
            dadosTr.appendChild(montaTd(pizza[4].toFixed(2), "tdMelhorCB" ));
        }
        
        return dadosTr;
    }
    
    function montaTd(dado, classe){
        let pizzaTd = document.createElement("td")
        pizzaTd.classList.add(classe);

        pizzaTd.textContent = dado;
        
        return pizzaTd;       
    }
}